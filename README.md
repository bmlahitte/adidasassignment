# Adidas Assignment
Selenium Test Automation Framework created to verify the expected behavior of https://www.adidas.co.uk site when the user searches for a category, selects a product and adds it to the bag.  

### Framework design
The framework was defined using Serenity and Cucumber to take advantage of the integration of these two tools to create 
a cleaner and readable code with the powerful reporting features provided by Serenity. 
*  /resources/features package ---> cucumber features
* /test/java/com.adidas/steps package ---> steps definitions
* /test/java/com.adidas/pages package ---> pages classes
* /test/java/com.adidas/commonSections package ---> sections that don't belong to an specific page

##### Environment Setup
* Maven 3.6.1
* JDK 11.0.2

##### Dependencies
* Cucumber 4.2.0
* SerenityBDD 2.4.34
* Junit 4.12

##### How to run
* mvn clean verify

##### Reports
The test results will be saved here: target/site/serenity/index.html
Reports can be seen in gitlab-ci, on the artifacts section. Here is the direct link to one of the build artifact: 
