package com.adidas.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;

public class SearchResultsPage extends PageObject {

    @FindBy(css = "a[data-auto-id*='glass-hockeycard-link']")
    List<WebElementFacade> results;

     By.ByCssSelector carousel = new By.ByCssSelector("div[class*='variation-carousel']");

    @FindBy(css = "p[data-auto-id*='product-card-title']")
    List<WebElementFacade> resultsName;

    public String getProductName(int position){
        waitForAnyRenderedElementOf(carousel);
        return resultsName.get(position).getText();
    }

    public void selectProductByOrder(int position){
        waitForAnyRenderedElementOf(carousel);
        results.get(position).click();
    }


}
