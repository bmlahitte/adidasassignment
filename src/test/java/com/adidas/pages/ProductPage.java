package com.adidas.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.NoSuchElementException;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

import java.util.List;

public class ProductPage extends PageObject {

    @FindBy(css = "a[class*=variation]")
    List<WebElementFacade> colorVariations;

    @FindBy(css = "button[data-di-id*='size']")
    List<WebElementFacade> sizes;

    @FindBy(css = "button[data-auto-id*=add-to-bag]")
    WebElementFacade addToBagBtn;

    @FindBy(css = "a[data-auto-id*=view-bag-desktop]")
    WebElementFacade viewBagBtn;

    @FindBy(css = "button[class*='selected']")
    WebElementFacade selectedSizeBtn;

    @FindBy(css = "h5[data-auto-id*='added-to-bag-modal-title']")
    WebElementFacade successfullyAddBagModalTitle;

    @FindBy(css = "div[data-auto-id*='bag-modal-product-quantity']")
    WebElementFacade successfullyAddBagModalProductQuantity;

    public void addProductToBag(){
        addToBagBtn.click();
    }

    public String getAddBagModalTitle(){
        return successfullyAddBagModalTitle.getText();
    }

    public String getProductQuantityOnSuccessfullyAddBagModal(){
        return successfullyAddBagModalProductQuantity.getText();
    }

    public void goToYourBagUsingViewBagBtn(){
        element(viewBagBtn).waitUntilVisible();
        viewBagBtn.click();
    }

    public void selectLastVariation(){
        colorVariations.get(colorVariations.size()-1).click();
        waitForWithRefresh();
    }

    public void selectSize(){
        String selectedSize = getSelectedSize();
        for (WebElementFacade sizeOption: sizes){
            if(!sizeOption.getText().equalsIgnoreCase(selectedSize)){
                sizeOption.click();
                return;
            }
        }

    }

    public String getSelectedSize(){
        String selectedSize = "";
        try{
            selectedSize = selectedSizeBtn.getText();
        }catch (NoSuchElementException e){
            e.printStackTrace();
        }
        return selectedSize;
    }

    public String getUrl(){
        return getDriver().getCurrentUrl();
    }
}
