package com.adidas.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import java.util.List;

@DefaultUrl("https://www.adidas.co.uk/cart")
public class YourBagPage extends PageObject {

    @FindBy(css = "h3[data-auto-id*='glass-cart-title']")
    WebElementFacade bagTitle;

    @FindBy(css = "img[data-auto-id*='glass-cart-line-item-image']")
    List<WebElementFacade> cartItems;

    @FindBy(css = "button[data-auto-id*='glass-cart-line-item-delete']")
    List<WebElementFacade> itemsDeleteBtn;

    @FindBy(css = "h3[data-auto-id*='glass-cart-empty-title']")
    WebElementFacade emptyBagTitle;

    public String getBagTitle(){
        element(bagTitle).waitUntilVisible();
        return bagTitle.getText();
    }

    public void goToProductPage(int productIndex){
        cartItems.get(productIndex).click();
    }

    public void removeAllItems(){
        for (WebElementFacade element: itemsDeleteBtn) {
            element.click();
        }
    }

    public String getEmptyBagTitle(){
        return emptyBagTitle.getText();
    }
}
