package com.adidas.pages;

import com.adidas.commonSections.ModalsHandler;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WhenPageOpens;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.support.ui.ExpectedConditions;

@DefaultUrl("https://www.adidas.co.uk/")
public class HomePage extends PageObject {

    @Steps
    ModalsHandler modalsHandler;

    @FindBy(css = "input[data-auto-id*='searchinput']")
    WebElementFacade searchField;

    public void searchForProduct(String product){
        clickOn(searchField);
        searchField.typeAndEnter(product);
        waitForCondition().until(ExpectedConditions
                .urlToBe("https://www.adidas.co.uk/search?q=PRODUCT"
                        .replace("PRODUCT", product.toLowerCase())));
    }

    @WhenPageOpens
    public void closeLocationModalPopUp(){
        modalsHandler.closeLocationModalPopUp();
    }

}
