package com.adidas.commonSections;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.WhenPageOpens;
import org.openqa.selenium.By;

public class ModalsHandler extends PageObject {

    @FindBy(css = "button[data-auto-id*='consent-accept-button']")
    WebElementFacade acceptTrackingBtn;

    @FindBy(css = "button[class*='gl-modal__close']")
    WebElementFacade locationModalCloseBtn;

    @FindBy(className = "gl-modal__close")
    WebElementFacade signUpModalCloseBtn;

    By.ByCssSelector locationModal = new By.ByCssSelector("div[class*='gl-modal__main-content']");

    @WhenPageOpens
    public void closeLocationModalPopUp(){
        waitForRenderedElementsToBePresent(locationModal);
        element(locationModalCloseBtn).waitUntilVisible();
        locationModalCloseBtn.click();
    }

    public void acceptTrackingPopUp(){
        element(acceptTrackingBtn).waitUntilVisible();
        acceptTrackingBtn.click();
    }

    public void closeSingUpModal(){
        element(signUpModalCloseBtn).waitUntilVisible();
        signUpModalCloseBtn.click();
    }
}
