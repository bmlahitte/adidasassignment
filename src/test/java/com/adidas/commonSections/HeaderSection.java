package com.adidas.commonSections;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class HeaderSection extends PageObject
{
    @FindBy(css = "a[class*='addtobag']")
    WebElementFacade goToBagHeaderIcon;

    public void goToYourBag(){
        goToBagHeaderIcon.click();
    }

}
