package com.adidas.steps;

import com.adidas.commonSections.HeaderSection;
import com.adidas.commonSections.ModalsHandler;
import com.adidas.pages.HomePage;
import com.adidas.pages.ProductPage;
import com.adidas.pages.SearchResultsPage;
import com.adidas.pages.YourBagPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class SearchingAndAddingToMyBag {

    @Managed
    WebDriver driver;

    @Steps
    HomePage homePage;

    @Steps
    SearchResultsPage searchResultsPage;

    @Steps
    ProductPage productPage;

    @Steps
    YourBagPage yourBagPage;

    @Steps
    HeaderSection headerSection;

    @Steps
    ModalsHandler modalsHandler;

    @Given("A user is searching for {string} products in the home page")
    public void a_user_is_searching_for_products_in_the_home_page(String product) {
        homePage.open();
        homePage.closeLocationModalPopUp();
        modalsHandler.acceptTrackingPopUp();
        homePage.searchForProduct(product);
        Assert.assertTrue(homePage
                .compatibleWithUrl("https://www.adidas.co.uk/search?q=PRODUCT"
                        .replace("PRODUCT", product.toLowerCase())));
    }

    @Given("Selects the {string} result on the list")
    public void selects_the_result_on_the_list(String position) {
        int index = Integer.parseInt(position);
        String productName = searchResultsPage.getProductName(index)
                .replace(" ", "-")
                .toLowerCase();
        searchResultsPage.selectProductByOrder(index);
        Assert.assertTrue(productPage.getUrl().contains(productName));
    }

    @Given("Update the color of the product with the last variation")
    public void update_the_color_of_the_product_with_the_variation() {
        modalsHandler.closeSingUpModal();
        String urlBeforeColorUpdate = productPage.getUrl();
        productPage.selectLastVariation();
        Assert.assertNotEquals(urlBeforeColorUpdate, productPage.getUrl());
    }

    @When("The user select a size")
    public void the_user_select_a_size() {
        productPage.selectSize();
    }

    @When("Adds the product to the shopping bag")
    public void add_the_product_to_the_shopping_bag() {
        productPage.addProductToBag();
        Assert.assertTrue(productPage.getAddBagModalTitle().equalsIgnoreCase("Successfully added to bag!"));
        Assert.assertTrue(productPage.getProductQuantityOnSuccessfullyAddBagModal()
                .equalsIgnoreCase("Quantity: 1"));
    }

    @When("Navigates to Your Bag section")
    public void navigates_to_the_your_bag_section() {
        productPage.goToYourBagUsingViewBagBtn();
        Assert.assertTrue(yourBagPage.getBagTitle().equalsIgnoreCase("Your Bag"));
    }

    @Then("The user should be able to edit the size of the product")
    public void the_user_should_be_able_to_edit_the_size_of_the_product() {
        yourBagPage.goToProductPage(0);
        String selectedSizeBeforeEdition = productPage.getSelectedSize();
        productPage.selectSize();
        Assert.assertFalse(productPage.getSelectedSize().equalsIgnoreCase(selectedSizeBeforeEdition));
    }

    @Then("Remove the product from the Bag")
    public void remove_the_product_from_the_bag() {
        headerSection.goToYourBag();
        yourBagPage.removeAllItems();
        Assert.assertTrue(yourBagPage.getEmptyBagTitle().equalsIgnoreCase("Your Bag is Empty"));
    }


}
