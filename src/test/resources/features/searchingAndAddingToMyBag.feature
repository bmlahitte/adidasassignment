Feature: Searching and adding a product to the shopping bag

  Scenario: Searching a product, adding it and removing it from the shopping bag
    Given A user is searching for "running" products in the home page
    And Selects the "0" result on the list
    And Update the color of the product with the last variation
    When The user select a size
    And Adds the product to the shopping bag
    And Navigates to Your Bag section
    Then The user should be able to edit the size of the product
    And Remove the product from the Bag